'use strict'

import React from 'react'
import { render } from 'react-dom'
import App from "./App/firstFile"

render(
<App />,
    document.getElementById('root')
)