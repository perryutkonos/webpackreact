'use strict';
import React, { Component, PropTypes } from 'react'

export default class App extends Component {

    render() {

        var  my_news = [
            {
                author: 'Саша Печкин',
                text: 'В четчерг, четвертого числа...',
                bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
            },
            {
                author: 'Просто Вася',
                text: 'Считаю, что $ должен стоить 35 рублей!',
                bigText: 'А евро 42!'
            },
            {
                author: 'Гость',
                text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
                bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
            }
        ];

        return (
            <div className='app'>
                <h3>Новости</h3>
                <News data={my_news} />
            </div>
        );
    }
}

class News extends Component{
    static propTypes = {
        data: React.PropTypes.array.isRequired
    }
    render() {
        var data = this.props.data;
        var newsTemplate;

        if (data.length > 0) {
            newsTemplate = data.map(function(item, index) {
                return (
                    <div key={index}>
                        <Article data={item} />
                    </div>
                )
            })
        } else {
            newsTemplate = <p>К сожалению новостей нет</p>
        }

        return (
            <div className='news'>
                {newsTemplate}
                <strong className={'news__count ' + (data.length > 0 ? '':'none') }>Всего новостей: {data.length}</strong>
            </div>
        );
    }
}

class Article extends Component{

    static propTypes = {
        data: React.PropTypes.shape({
            author: React.PropTypes.string.isRequired,
            text: React.PropTypes.string.isRequired,
            bigText: React.PropTypes.string.isRequired
        })
    }

    state = {
        visible: false
    };

    readmoreClick = (e) => {
        this.setState({visible: true});
    }

    render(){
        var author = this.props.data.author,
            text = this.props.data.text,
            bigText = this.props.data.bigText,
            visible = this.state.visible;

        return (
            <div className='article'>
                <p className='news__author'>{author}:</p>
                <p className='news__text'>{text}</p>
                <a href="#"
                   onClick={this.readmoreClick}
                   className={'news__readmore ' + (visible ? 'none': '')}>
                    Подробнее
                </a>
                <p className={'news__big-text ' + (visible ? '': 'none')}>{bigText}</p>
            </div>
        )
    }

}

