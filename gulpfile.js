'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    stylus = require('gulp-stylus'),
    jade = require('gulp-jade'),
    concat = require('gulp-concat'), //конкатинация файлов
    filter = require('gulp-filter'),
    webpack = require('gulp-webpack');


var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        images: 'build/images/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.jade',
        js: 'src/js/index.js',
        style: 'src/style/main.scss',
        img: 'src/img/**/*.*',
        images: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.jade',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.styl',
        img: 'src/img/**/*.*',
        images: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
};

gulp.task('webserver', function () {
    //browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    /* gulp.src(path.src.html)
     .pipe(rigger())
     .pipe(gulp.dest(path.build.html))*/

    gulp.src(path.src.html)
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(path.build.html));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('picture:build', function() {
    gulp.src(path.src.images)
        .pipe(gulp.dest(path.build.images))
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('js:build', function () {

    gulp.src(path.src.js)
        .pipe(webpack({
            watch: true,
            devtool: "source-map",
            watchOptions: {
                aggregateTimeout: 100
            },
            output: {
                path: __dirname + '/App',
                filename: 'bundle.js',
            },
            module: { //Обновлено
                loaders: [ //добавили babel-loader
                    {
                        test: /\.js?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            "presets": ["es2015", "stage-0", "react"]
                        }
                    }
                ]
            }
        }))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        /*.pipe(stylus())*/
        .pipe(sass({
            includePaths: ['src/style/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css));
});

//эта таска берет все моудли из bower.json объединяет в один файл vendor.js скрипты, кладет их в паку дистрибутива
// берет все файлы, кладет их в _vendor.scss в папке приложения. Потом этот файл будет включаться другим таском в итоговый
gulp.task('bower', function () {
    var bowerFiles = require('main-bower-files')({
        checkExistence: true
    });
    console.log(bowerFiles);
    var jsFilter = filter(function (file) {
        return file.path.match(/\.(js)$/i);
    });
    var cssFilter = filter(function (file) {
        return file.path.match(/\.(css)$/i);
    });
    gulp.src(bowerFiles)
        .pipe(jsFilter)
        .pipe(concat('_libraries.js'))
        .pipe(gulp.dest(path.build.js));
    //.pipe(jsFilter.restore())
    gulp.src(bowerFiles)
        .pipe(cssFilter)
        .pipe(concat('_libraries.css'))
        .pipe(gulp.dest('src/style/modules/'));
});

gulp.task('build', [
    'html:build',
    'js:build',
    //'bower',
    'style:build',
    'fonts:build',
    'image:build',
    'picture:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.images], function(event, cb) {
        gulp.start('picture:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);